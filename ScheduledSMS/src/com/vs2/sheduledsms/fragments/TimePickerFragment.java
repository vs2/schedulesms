package com.vs2.sheduledsms.fragments;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.TimePicker;

import com.vs2.sheduledsms.dialogs.SheduledSmsDialog;

public class TimePickerFragment extends DialogFragment implements
		OnTimeSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		int hour;
		int minute;
		
		if (SheduledSmsDialog.hour == 0 || SheduledSmsDialog.minute == 0) {
			final Calendar c = Calendar.getInstance();
			hour = c.get(Calendar.HOUR_OF_DAY);
			minute = c.get(Calendar.MINUTE);
		} else {
			hour = SheduledSmsDialog.hour;
			minute = SheduledSmsDialog.minute;
		}

		// Create a new instance of TimePickerDialog and return it
		/*
		 * return new TimePickerDialog(getActivity(), this, hour, minute,
		 * DateFormat.is24HourFormat(getActivity()));
		 */
	
		return new TimePickerDialog(getActivity(), this, hour, minute, true);
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		SheduledSmsDialog.hour = hourOfDay;
		SheduledSmsDialog.minute = minute;
		SheduledSmsDialog.setTime();
	}

}
