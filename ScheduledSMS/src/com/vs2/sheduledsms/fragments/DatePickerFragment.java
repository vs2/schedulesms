package com.vs2.sheduledsms.fragments;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.vs2.sheduledsms.dialogs.SheduledSmsDialog;
import com.vs2.sheduledsms.utilities.Logcat;

public class DatePickerFragment extends DialogFragment implements
		OnDateSetListener, OnDateChangedListener {

	private Calendar maxDate;

	@SuppressLint("NewApi")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		int year;
		int month;
		int day;
		Calendar cal = Calendar.getInstance();
		if (SheduledSmsDialog.day == 0 || SheduledSmsDialog.month == 0
				|| SheduledSmsDialog.year == 0) {
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH);
			day = cal.get(Calendar.DAY_OF_MONTH);
		} else {
			year = SheduledSmsDialog.year;
			month = SheduledSmsDialog.month - 1;
			day = SheduledSmsDialog.day;
		}

		DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
				this, year, month, day);

		DatePicker datePicker = datePickerDialog.getDatePicker();
		cal.add(Calendar.YEAR, -10);
		maxDate = cal;

		Logcat.e("MaxDate day", "" + maxDate.getTime());

		datePicker.setMinDate(maxDate.getTimeInMillis());
		
		// datePicker.setMaxDate(maxDate.getTimeInMillis());

		return datePickerDialog;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		SheduledSmsDialog.day = dayOfMonth;
		SheduledSmsDialog.month = monthOfYear + 1;
		SheduledSmsDialog.year = year;
		SheduledSmsDialog.setDOB();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(bundle);
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Calendar newDate = Calendar.getInstance();
			newDate.set(year, monthOfYear, dayOfMonth);
			if (maxDate != null && maxDate.before(newDate)) {
				view.init(maxDate.get(Calendar.YEAR),
						maxDate.get(Calendar.MONTH),
						maxDate.get(Calendar.DAY_OF_MONTH), this);
				// setTitle(mTitleDateFormat.format(maxDate.getTime()));
			} else {
				view.init(year, monthOfYear, dayOfMonth, this);
				// setTitle(mTitleDateFormat.format(newDate.getTime()));
			}
		}
	}



}