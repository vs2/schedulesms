package com.vs2.sheduledsms.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.adpaters.SheduledsSMSAdapter;
import com.vs2.sheduledsms.alarm.SheduledAalarmReceiver;
import com.vs2.sheduledsms.alarm.SheduledAlaramManger;
import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.dialogs.SheduledSmsDialog;
import com.vs2.sheduledsms.interfaces.OnSmsDialogClick;
import com.vs2.sheduledsms.interfaces.SendSmsListener;
import com.vs2.sheduledsms.interfaces.onSelectContact;
import com.vs2.sheduledsms.objects.ContactPicker;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.quickmenu.ActionItem;
import com.vs2.sheduledsms.quickmenu.QuickAction;
import com.vs2.sheduledsms.utilities.Logcat;

public class ScheduleFragment extends Fragment implements OnSmsDialogClick,
		SendSmsListener, onSelectContact {

	public static ArrayList<SheduledSms> mSmsList;
	public static SheduledsSMSAdapter mSheduledAdapter;
	public static ListView mListViewSms;
	public static TextView mTextViewErrorMessage;
	public static SheduledSmsDialog smsDialog;
	private TextView textViewErrorMessage;
	private static final int ID_EDIT = 1;
	private static final int ID_DELETE = 2;

	private View mView;

	public static Context mContext;
	
	private Button buttonAdd;
	private android.app.ActionBar mActionbar;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.schedule_fragment, container, false);
		mContext = getActivity();
		initGlobal();
		mActionbar = getActivity().getActionBar();
		mActionbar.setTitle("Schedule SMS");
		showList(false);
		new getSheduledSmsList().execute();
		SheduledAalarmReceiver.setListner(this);
		return mView;
	}

	private void initGlobal() {
		textViewErrorMessage = (TextView) mView.findViewById(R.id.textViewErrorMessage);
		textViewErrorMessage.setText(mContext.getResources().getString(R.string.no_sheduled_sms_found));
		buttonAdd = (Button) mView.findViewById(R.id.buttonAdd);
		buttonAdd.setVisibility(View.VISIBLE);
		mListViewSms = (ListView) mView.findViewById(R.id.listViewSmsList);
		mTextViewErrorMessage = (TextView) mView
				.findViewById(R.id.textViewErrorMessage);

		mListViewSms
				.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(android.widget.AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						// TODO Auto-generated method stub
						showSimpleActionView(arg1, arg2);
					}
				});
		
		buttonAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				smsDialog = new SheduledSmsDialog(getActivity());
				smsDialog.show();
				smsDialog.setType(Globals.SAVE, null, getFragmentManager());
			}
		});
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	public static void loadSmsList() {
		// mSheduledAdapter = new SheduledSmsAdapter(MainActivity.this,
		// mSmsList);
		mSheduledAdapter = new SheduledsSMSAdapter(mContext, mSmsList);
		mListViewSms.setAdapter(mSheduledAdapter);

	}

	public static void showList(boolean status) {
		if (status) {
			mListViewSms.setVisibility(View.VISIBLE);
			mTextViewErrorMessage.setVisibility(View.GONE);
		} else {
			mListViewSms.setVisibility(View.GONE);
			mTextViewErrorMessage.setVisibility(View.VISIBLE);
		}
	}

	public static class getSheduledSmsList extends
			AsyncTask<Void, Void, ArrayList<SheduledSms>> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(mContext);
			pd.setMessage("Please wait, getting data");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected ArrayList<SheduledSms> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				
				//return DatabaseFunctions.getSheduledSms(Globals.ALL_RECORDS);
				return DatabaseFunctions.getSheduledTabSms();
				
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("getSheduledSmsList doInBackground", "" + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(ArrayList<SheduledSms> result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				if (result != null && result.size() > 0) {
					mSmsList = result;
					showList(true);
					loadSmsList();
				} else {
					showList(false);
				}
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("getSheduledSmsList onPostExecute", "" + e.toString());
			}
			super.onPostExecute(result);
		}
	}

	public static void startAlaram(SheduledSms sheduledSms) {

		int day, month, year, hour, minute;

		String[] date = sheduledSms.getDate().toString().trim().split("-");

		String[] time = sheduledSms.getTime().toString().trim().split(":");

		day = Integer.parseInt(date[0]);
		month = Integer.parseInt(date[1]);
		year = Integer.parseInt(date[2]);

		hour = Integer.parseInt(time[0]);
		minute = Integer.parseInt(time[1]);

		try {

			int alarmId = Integer.parseInt("" + SheduledAlaramManger.ALARM_ID
					+ "" + sheduledSms.getId());
			SheduledAlaramManger.cancelAlaram(mContext, alarmId);

		} catch (Exception e) {
			// TODO: handle exception
		}
		SheduledAlaramManger.setAlaram(mContext, day, month, year, hour,
				minute, sheduledSms.getId());
	}

	public static void sendSheduleSMS(final SheduledSms sheduleSms,
			final Context context) {

		String SENT = "SMS_SENT";

		PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,new Intent(SENT), 0);

		// ---when the SMS has been sent---
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					try {
						DatabaseFunctions.updateSheduleSmsStatus(
								sheduleSms.getId(), Globals.SUCCESS);
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result Ok DatabaseFunctions", "" + e.toString());
					}
					try {
						new getSheduledSmsList().execute();
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result Ok Notify error", "" + e.toString());
					}
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					try {
						DatabaseFunctions.updateSheduleSmsStatus(
								sheduleSms.getId(), Globals.FAILED);
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result RESULT_ERROR_GENERIC_FAILURE DatabaseFunctions", "" + e.toString());
					}
					try {
						new getSheduledSmsList().execute();
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result Ok Notify error", "" + e.toString());
					}
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					try {
						DatabaseFunctions.updateSheduleSmsStatus(
								sheduleSms.getId(), Globals.FAILED);
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result RESULT_ERROR_NO_SERVICE DatabaseFunctions", "" + e.toString());
					}
					try {
						new getSheduledSmsList().execute();
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("RESULT_ERROR_NO_SERVICE Ok Notify error", ""
								+ e.toString());
					}
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					try {
						DatabaseFunctions.updateSheduleSmsStatus(
								sheduleSms.getId(), Globals.FAILED);
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result RESULT_ERROR_NULL_PDU DatabaseFunctions", "" + e.toString());
					}
					try {
						new getSheduledSmsList().execute();
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("RESULT_ERROR_NULL_PDU Ok Notify error", ""
								+ e.toString());
					}
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					try {
						DatabaseFunctions.updateSheduleSmsStatus(
								sheduleSms.getId(), Globals.FAILED);
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("Result RESULT_ERROR_RADIO_OFF DatabaseFunctions", "" + e.toString());
					}
					try {
						new getSheduledSmsList().execute();
					} catch (Exception e) {
						// TODO: handle exception
						Logcat.e("RESULT_ERROR_RADIO_OFF Ok Notify error", ""
								+ e.toString());
					}
					break;
				}
			}
		}, new IntentFilter(SENT));
		SmsManager sms = SmsManager.getDefault();
		// sms.sendTextMessage(phoneNumber, null, message, sentPI, null);
		// phoneNumber = "9033759806";
		sms.sendTextMessage(sheduleSms.getSmsTo(), null,
				sheduleSms.getMessage(), sentPI, null);
	}

	public static void getSelected_Contact(ContactPicker contact) {
		if (contact != null) {
			ScheduleFragment.smsDialog.setContactNumber(contact);
		} else {
			Toast.makeText(mContext, "No Contact Found", Toast.LENGTH_LONG)
					.show();
		}
	}

	public static void send_sms(SheduledSms sheduledSms, Context context) {
		Logcat.e("shedule fragment", "sheduledSms: " + sheduledSms.toString());
		sendSheduleSMS(sheduledSms, context);
	}

	public static void onDialog_Click(int type, SheduledSms sheduledSms) {
		if (type != Globals.ERROR) {
			Logcat.e("Latest Data", "" + sheduledSms.getId() + ":"
					+ sheduledSms.getSmsTo());
			new getSheduledSmsList().execute();

			startAlaram(sheduledSms);
		}
	}

	@Override
	public void getSelectedContact(ContactPicker contact) {
		// TODO Auto-generated method stub
		if (contact != null) {
			ScheduleFragment.smsDialog.setContactNumber(contact);
		} else {
			Toast.makeText(getActivity(), "No Contact Found", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	public void sendsms(SheduledSms sheduledSms) {
		// TODO Auto-generated method stub
		sendSheduleSMS(sheduledSms, getActivity());
	}

	@Override
	public void onDialogClick(int type, SheduledSms sheduledSms) {
		// TODO Auto-generated method stub
		if (type != Globals.ERROR) {
			Logcat.e("Latest Data", "" + sheduledSms.getId() + ":"
					+ sheduledSms.getSmsTo());
			new getSheduledSmsList().execute();

			startAlaram(sheduledSms);
		}
	}

	private void showSimpleActionView(View view, final int position) {

		final QuickAction quickAction = new QuickAction(getActivity(),
				QuickAction.VERTICAL);

		ActionItem individualPassword = new ActionItem(ID_EDIT, "Edit",
				getResources().getDrawable(R.drawable.ic_action_holo_dark_edit));
		ActionItem commonPassword = new ActionItem(ID_DELETE, "Remove",
				getResources().getDrawable(
						R.drawable.ic_action_holo_dark_remove));
		quickAction.addActionItem(individualPassword);
		quickAction.addActionItem(commonPassword);

		// Set listener for action item clicked
		quickAction
				.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
					@SuppressWarnings("unused")
					@Override
					public void onItemClick(QuickAction source, int pos,
							int actionId) {
						ActionItem actionItem = quickAction.getActionItem(pos);

						// here we can filter which action item was clicked with
						// pos or actionId parameter

						int action = actionId;

						switch (action) {
						case ID_EDIT:
							// if(smsDialog == null){
							ScheduleFragment.smsDialog = new SheduledSmsDialog(getActivity());
							ScheduleFragment.smsDialog.show();
							ScheduleFragment.smsDialog.setType(Globals.UPDATE,
									mSmsList.get(position),
									getFragmentManager());
							/*
							 * }else{ smsDialog.setType(Globals.UPDATE,
							 * mSmsList.get(position),
							 * getSupportFragmentManager()); }
							 */
							break;
						case ID_DELETE:
							openDeleteDialog(mSmsList.get(position).getId());
							break;
						default:
							break;
						}
					}
				});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
		quickAction.show(view);
	}

	private void openDeleteDialog(final int smsId) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
				getActivity());
		dialogBuilder
				.setMessage("Do you want to delete this shedule?")
				.setTitle(R.string.app_name)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								if (DatabaseFunctions.deleteTemplate(smsId)) {
									Toast.makeText(getActivity(),
											"Shedule Deleted.",
											Toast.LENGTH_LONG).show();
									new getSheduledSmsList().execute();
									try {
										int alarmId = Integer.parseInt(""
												+ SheduledAlaramManger.ALARM_ID
												+ "" + smsId);
										SheduledAlaramManger.cancelAlaram(
												getActivity(), alarmId);
									} catch (Exception e) {
										// TODO: handle exception
									}
								} else {
									Toast.makeText(getActivity(),
											"Unable to delete shedule.",
											Toast.LENGTH_LONG).show();
								}
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
		AlertDialog dialog = dialogBuilder.create();
		dialog.show();
	}

}
