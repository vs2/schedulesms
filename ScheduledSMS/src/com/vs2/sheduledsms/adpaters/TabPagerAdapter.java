package com.vs2.sheduledsms.adpaters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vs2.scheduledsms.MainActivity;
import com.vs2.sheduledsms.fragments.ScheduleFragment;
import com.vs2.sheduledsms.fragments.SentFragment;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
	
	
    public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub	
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
        case 0:
            //Fragement for Status Tab
            return new ScheduleFragment();
        case 1:
           //Fragment for Settings Tab
            return new SentFragment();
        }
		return null;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2; //No of Tabs
	}


    }