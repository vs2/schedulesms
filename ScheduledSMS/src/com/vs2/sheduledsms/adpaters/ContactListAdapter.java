package com.vs2.sheduledsms.adpaters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.dialogs.ContactListDialog;
import com.vs2.sheduledsms.objects.ContactPicker;
import com.vs2.sheduledsms.utilities.Logcat;
import com.vs2.sheduledsms.utilities.Utility;

@SuppressLint("DefaultLocale")
public class ContactListAdapter extends BaseAdapter implements Filterable {

	ArrayList<ContactPicker> mContactList;
	private ArrayList<ContactPicker> DisplayContactList;
	LayoutInflater mInflater;
	Context mContext;

	public ContactListAdapter(Context context,
			ArrayList<ContactPicker> contactlist) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mInflater = LayoutInflater.from(mContext);
		mContactList = contactlist;
		this.DisplayContactList = contactlist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mContactList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mContactList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.contact_list_item, null);

			holder = new ViewHolder();
			holder.textName = (TextView) convertView
					.findViewById(R.id.textViewName);
			holder.textNumber = (TextView) convertView
					.findViewById(R.id.textViewNumber);
			holder.imageViewPic = (CircularImageView) convertView
					.findViewById(R.id.image_contact);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textName.setText("" + mContactList.get(position).getName());

		holder.textNumber.setText("" + mContactList.get(position).getNumber());

		Bitmap bitmap = Utility.getByteContactPhoto(mContactList.get(position)
				.getId(), mContext);

		if (bitmap != null) {
			holder.imageViewPic.setImageBitmap(bitmap);
		} else {
			holder.imageViewPic.setImageResource(R.drawable.ic_launcher);
		}

		return convertView;
	}

	public class ViewHolder {
		TextView textName, textNumber;
		CircularImageView imageViewPic;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub

		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				Logcat.e("PublishResults Constraint",
						"" + constraint.toString());
				mContactList = (ArrayList<ContactPicker>) results.values;
				ContactListDialog.mContactPickerList = mContactList;
				notifyDataSetChanged();
			}

			@SuppressLint("DefaultLocale")
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				FilterResults results = new FilterResults();
				ArrayList<ContactPicker> FilteredArrList = new ArrayList<ContactPicker>();

				mContactList = new ArrayList<ContactPicker>(DisplayContactList);

				if (constraint == null || constraint.length() == 0) {

					// set the Original result to return
					results.count = mContactList.size();
					results.values = mContactList;
				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < mContactList.size(); i++) {
						String data = mContactList.get(i).getName();
						if (data.toLowerCase()
								.startsWith(constraint.toString())) {
							FilteredArrList.add(mContactList.get(i));
						} else if (data.toLowerCase().contains(
								constraint.toString())) {
							FilteredArrList.add(mContactList.get(i));
						}
					}
					// set the Filtered result to return
					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};

		return filter;
	}
}
