package com.vs2.sheduledsms.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;

public class SheduledSmsAdapter extends BaseAdapter {

	ArrayList<SheduledSms> mSmsList;
	LayoutInflater mInflater;
	Context mContext;

	public SheduledSmsAdapter(Context context, ArrayList<SheduledSms> smsList) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mInflater = LayoutInflater.from(mContext);
		mSmsList = smsList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mSmsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mSmsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.sms_list_item, null);

			holder = new ViewHolder();
			holder.textSmsTo = (TextView) convertView
					.findViewById(R.id.textViewSmsTo);
			holder.textMessage = (TextView) convertView
					.findViewById(R.id.textViewMessage);
			holder.textDate = (TextView) convertView
					.findViewById(R.id.textViewDate);
			holder.textStatus = (TextView) convertView
					.findViewById(R.id.textViewStatus);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textSmsTo.setText("smsTo: " + mSmsList.get(position).getSmsTo());

		String message = mSmsList.get(position).getMessage();

		if (message.length() > 0) {
			if (message.length() >= 30) {
				String newMessage = message.substring(0, 30);
				holder.textMessage.setText("" + newMessage+"...");
			} else {
				holder.textMessage.setText(""
						+ mSmsList.get(position).getMessage());
			}
		} else {
			holder.textMessage
					.setText("" + mSmsList.get(position).getMessage());
		}

		holder.textDate.setText("Sheduled: " + mSmsList.get(position).getDate()
				+ " " + mSmsList.get(position).getTime());

		int Status = mSmsList.get(position).getStatus();

		switch (Status) {
		case Globals.IN_QUEUE:
			holder.textStatus.setText("" + Globals.TEXT_INQUEUE);
			holder.textStatus.setTextColor(mContext.getResources().getColor(R.color.blue));
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.background_light_blue));
			break;
		case Globals.SUCCESS:
			holder.textStatus.setText("" + Globals.TEXT_SUCCESS);
			holder.textStatus.setTextColor(mContext.getResources().getColor(R.color.green));
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.background_light_green));
			break;
		case Globals.FAILED:
			holder.textStatus.setText("" + Globals.TEXT_FAILED);
			holder.textStatus.setTextColor(mContext.getResources().getColor(R.color.red));
			convertView.setBackgroundColor(mContext.getResources().getColor(R.color.background_light_red));
			break;
		default:
			break;
		}

		return convertView;
	}

	public class ViewHolder {
		TextView textSmsTo, textMessage, textDate, textStatus;
	}
}
