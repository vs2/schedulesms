package com.vs2.sheduledsms.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.utilities.Utility;

public class SheduledsSMSAdapter extends BaseAdapter {

	ArrayList<SheduledSms> mSmsList;
	LayoutInflater mInflater;
	Context mContext;

	public SheduledsSMSAdapter(Context context, ArrayList<SheduledSms> smsList) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mInflater = LayoutInflater.from(mContext);
		mSmsList = smsList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mSmsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mSmsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.sms_list_items, null);

			holder = new ViewHolder();
			holder.textContact = (TextView) convertView
					.findViewById(R.id.textview_contactname);
			holder.textMessage = (TextView) convertView
					.findViewById(R.id.textview_message);
			holder.texttime = (TextView) convertView
					.findViewById(R.id.textview_time);
			holder.imageViewContact = (CircularImageView) convertView
					.findViewById(R.id.image_contact);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textContact.setText(mSmsList.get(position).getSmsTo());

		String message = mSmsList.get(position).getMessage();

		if (message.length() > 0) {
			if (message.length() >= 30) {
				String newMessage = message.substring(0, 30);
				holder.textMessage.setText("" + newMessage + "...");
			} else {
				holder.textMessage.setText(""
						+ mSmsList.get(position).getMessage());
			}
		} else {
			holder.textMessage
					.setText("" + mSmsList.get(position).getMessage());
		}

		holder.texttime.setText(mSmsList.get(position).getDate() + " "
				+ mSmsList.get(position).getTime());

		Bitmap bitmap=null;
		if(mSmsList.get(position).getContactId()!=null)
		{
		bitmap = Utility.getByteContactPhoto(mSmsList.get(position)
				.getContactId(), mContext);
		}

		if (bitmap != null) {
			holder.imageViewContact.setImageBitmap(bitmap);
		} else {
			holder.imageViewContact.setImageResource(R.drawable.ic_launcher);
		}

		int Status = mSmsList.get(position).getStatus();

		switch (Status) {
		case Globals.IN_QUEUE:
			holder.texttime.setBackgroundColor(mContext.getResources()
					.getColor(R.color.dark_blue));
			break;
		case Globals.FAILED:
			holder.texttime.setBackgroundColor(mContext.getResources()
					.getColor(R.color.dark_red));
			break;
		default:
			break;
		}

		return convertView;
	}

	public class ViewHolder {
		TextView textContact, textMessage, texttime;
		CircularImageView imageViewContact;
	}
}
