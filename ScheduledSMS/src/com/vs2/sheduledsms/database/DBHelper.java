package com.vs2.sheduledsms.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.utilities.Logcat;

public class DBHelper extends SQLiteOpenHelper {

	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "applockDB";

	public static final String TABLE_SHEDULEDSMS = "sheduledsms";

	public static final String TAG = DBHelper.class.getSimpleName();

	@SuppressWarnings("unused")
	private Context mContext;

	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		String createTable;

		createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SHEDULEDSMS + "("
				+ SheduledSms._Id
				+ " integer primary key autoincrement, "
				+SheduledSms._smsTo+" VARCHAR,"
				+SheduledSms._Date+" VARCHAR,"
				+SheduledSms._Time+" VARCHAR,"
				+SheduledSms._message+" VARCHAR,"
				+SheduledSms._status+" integer,"
				+SheduledSms._contactId+" VARCHAR);";

		db.execSQL(createTable);

		Logcat.e(TAG, "Database created successfully!");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHEDULEDSMS);
		this.onCreate(db);
		Logcat.e(TAG, "Database updated successfully!");
	}

}
