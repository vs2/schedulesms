package com.vs2.sheduledsms.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.utilities.Logcat;

public class DatabaseFunctions {

	public static SQLiteDatabase db;
	public static DBHelper dbHelper;

	public static void openDB(Context context) {
		dbHelper = new DBHelper(context);
		if (db != null) {
			if (!db.isOpen()) {
				db = dbHelper.getWritableDatabase();
			}
		} else {
			db = dbHelper.getWritableDatabase();
		}
	}

	public static void closeDB() {
		if (db.isOpen()) {
			db.close();
			dbHelper.close();
		}
	}

	// Application Master
	public static ArrayList<SheduledSms> getSheduledSms(int status) {
		ArrayList<SheduledSms> smsList = new ArrayList<SheduledSms>();
		Cursor cursor = null;
		try {

			if (status == Globals.ALL_RECORDS) {
				cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
						+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
						+ SheduledSms._Time + "," + SheduledSms._message + ","
						+ SheduledSms._status + "," + SheduledSms._contactId
						+ " FROM " + DBHelper.TABLE_SHEDULEDSMS, null);
				
			} else {
				cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
						+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
						+ SheduledSms._Time + "," + SheduledSms._message + ","
						+ SheduledSms._status + "," + SheduledSms._contactId
						+ " FROM " + DBHelper.TABLE_SHEDULEDSMS + " where "
						+ SheduledSms._status + " = " + status, null);
			}

			while (cursor.moveToNext()) {

				SheduledSms sms = new SheduledSms(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getInt(5), false, cursor.getString(6));

				smsList.add(sms);
			}
			return smsList;
		} catch (Exception e) {
			Logcat.e("getSheduledSms SELECT", "error : " + e.toString());
			return smsList;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
	}

	public static ArrayList<SheduledSms> getSheduledTabSms() {
		ArrayList<SheduledSms> smsList = new ArrayList<SheduledSms>();
		Cursor cursor = null;
		try {

			cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
					+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
					+ SheduledSms._Time + "," + SheduledSms._message + ","
					+ SheduledSms._status + "," + SheduledSms._contactId
					+ " FROM " + DBHelper.TABLE_SHEDULEDSMS + " where "
					+ SheduledSms._status + " = " + Globals.IN_QUEUE + " OR "
					+ SheduledSms._status + " = " + Globals.FAILED, null);

			while (cursor.moveToNext()) {

				SheduledSms sms = new SheduledSms(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getInt(5), false, cursor.getString(6));

				smsList.add(sms);
			}
			return smsList;
		} catch (Exception e) {
			Logcat.e("getSheduledSms SELECT", "error : " + e.toString());
			return smsList;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
	}

	public static SheduledSms getSmsInfoFromDateTime(String date, String time) {
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
					+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
					+ SheduledSms._Time + "," + SheduledSms._message + ","
					+ SheduledSms._status + "," + SheduledSms._contactId
					+ " FROM " + DBHelper.TABLE_SHEDULEDSMS + " where "
					+ SheduledSms._Date + " = '" + date + "' AND "
					+ SheduledSms._Time + " = '" + time + ",", null);

			while (cursor.moveToNext()) {

				SheduledSms sms = new SheduledSms(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getInt(5), false, cursor.getString(6));
				return sms;
			}
			return null;
		} catch (Exception e) {
			Logcat.e("getSheduledSms SELECT", "error : " + e.toString());
			return null;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
	}

	public static SheduledSms getSmsFromId(int Id) {
		Cursor cursor = null;
		try {
			Logcat.e("SELECT", "getSmsFromId" );
			cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
					+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
					+ SheduledSms._Time + "," + SheduledSms._message + ","
					+ SheduledSms._status + "," + SheduledSms._contactId
					+ " FROM " + DBHelper.TABLE_SHEDULEDSMS + " where "
					+ SheduledSms._Id + " = " + Id, null);

			while (cursor.moveToNext()) {
				Logcat.e("getSheduledSms Data", " :0) " +cursor.getInt(0)+" :1) "+cursor.getString(1)+" :2) "+ cursor.getString(2)+" :3) "+ cursor.getString(3)+" :4) "+ cursor.getString(4)+" :5) "+ cursor.getInt(5)+" :6) "+ cursor.getString(6) );
				SheduledSms sms = new SheduledSms(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getInt(5), false, cursor.getString(6));
				Logcat.e("return sms", "return sms");
				return sms;
			}
			Logcat.e("return null", "return null");
			return null;
		} catch (Exception e) {
			Logcat.e("getSheduledSms SELECT", "error : " + e.toString());
			return null;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
	}

	public static SheduledSms getSmsMaxRecord() {
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT  " + SheduledSms._Id + ","
					+ SheduledSms._smsTo + "," + SheduledSms._Date + ","
					+ SheduledSms._Time + "," + SheduledSms._message + ","
					+ SheduledSms._status + "," + SheduledSms._contactId
					+ " FROM " + DBHelper.TABLE_SHEDULEDSMS + " ORDER BY "
					+ SheduledSms._Id + " DESC ", null);

			while (cursor.moveToNext()) {

				SheduledSms sms = new SheduledSms(cursor.getInt(0),
						cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getInt(5), false,cursor.getString(6));
				return sms;
			}
			return null;
		} catch (Exception e) {
			Logcat.e("getSheduledSms SELECT", "error : " + e.toString());
			return null;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
	}

	public static boolean saveSheduledSms(String smsTo, String Date,
			String Time, String message, Integer status,String contactId) {

		ContentValues values;
		try {
			values = new ContentValues();
			values.put(SheduledSms._smsTo, smsTo);
			values.put(SheduledSms._Date, Date);
			values.put(SheduledSms._Time, Time);
			values.put(SheduledSms._message, message);
			values.put(SheduledSms._status, "" + status);
			values.put(SheduledSms._contactId, contactId);
			db.insert(DBHelper.TABLE_SHEDULEDSMS, null, values);
			return true;
		} catch (Exception e) {
			Logcat.e("TABLE_SHEDULEDSMS INSERT", "error : " + e.toString());
			return false;
		}
	}

	public static boolean updateSheduleSms(int Id, String smsTo, String Date,
			String Time, String message) {
		ContentValues values;
		try {
			values = new ContentValues();
			values.put(SheduledSms._smsTo, smsTo);
			values.put(SheduledSms._Date, Date);
			values.put(SheduledSms._Time, Time);
			values.put(SheduledSms._message, message);

			db.update(DBHelper.TABLE_SHEDULEDSMS, values, "" + SheduledSms._Id
					+ " =?", new String[] { String.valueOf(Id) });
			Logcat.e("TABLE_SHEDULEDSMS Update", "Record Updated!");
			return true;
		} catch (Exception e) {
			Logcat.e("TABLE_SHEDULEDSMS Update", "error : " + e.toString());
			return false;
		}
	}

	public static boolean updateSheduleSmsStatus(int Id, int status) {
		ContentValues values;
		try {
			values = new ContentValues();
			values.put(SheduledSms._status, status);

			db.update(DBHelper.TABLE_SHEDULEDSMS, values, "" + SheduledSms._Id
					+ " =?", new String[] { String.valueOf(Id) });
			Logcat.e("TABLE_SHEDULEDSMS Update", "Record Updated!");
			return true;
		} catch (Exception e) {
			Logcat.e("TABLE_SHEDULEDSMS Update", "error : " + e.toString());
			return false;
		}
	}

	public static boolean deleteTemplate(int id) {
		try {
			db.execSQL("delete from " + DBHelper.TABLE_SHEDULEDSMS + " where "
					+ SheduledSms._Id + " = " + id);
			return true;
		} catch (Exception e) {
			Logcat.e("TABLE_SHEDULEDSMS deleted", "error : " + e.toString());
			return false;
		}
	}

}
