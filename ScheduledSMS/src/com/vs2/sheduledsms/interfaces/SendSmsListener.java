package com.vs2.sheduledsms.interfaces;

import com.vs2.sheduledsms.objects.SheduledSms;

public interface SendSmsListener {

	public void sendsms(SheduledSms sheduledSms);

}
