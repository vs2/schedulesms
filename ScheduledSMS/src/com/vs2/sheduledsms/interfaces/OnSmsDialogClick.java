package com.vs2.sheduledsms.interfaces;

import com.vs2.sheduledsms.objects.SheduledSms;

public interface OnSmsDialogClick {

	public void onDialogClick(int type, SheduledSms sheduledSms);

}
