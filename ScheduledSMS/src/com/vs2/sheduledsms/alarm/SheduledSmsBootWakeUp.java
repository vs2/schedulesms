package com.vs2.sheduledsms.alarm;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;

public class SheduledSmsBootWakeUp extends BroadcastReceiver {

	// private static final String tag = "NotifyBootWakeUp";

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		try {
			DatabaseFunctions.openDB(context);
		} catch (Exception e) {
			// TODO: handle exception
		}

		ArrayList<SheduledSms> smsList = DatabaseFunctions
				.getSheduledSms(Globals.IN_QUEUE);

		if (smsList != null && smsList.size() > 0) {
			for (int i = 0; i < smsList.size(); i++) {
				try {
					startAlaram(smsList.get(i), context);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		}

	}

	public void startAlaram(SheduledSms sheduledSms, Context context) {

		int day, month, year, hour, minute;

		String[] date = sheduledSms.getDate().toString().trim().split("-");

		String[] time = sheduledSms.getTime().toString().trim().split(":");

		day = Integer.parseInt(date[0]);
		month = Integer.parseInt(date[1]);
		year = Integer.parseInt(date[2]);

		hour = Integer.parseInt(time[0]);
		minute = Integer.parseInt(time[1]);

		try {

			int alarmId = Integer.parseInt("" + SheduledAlaramManger.ALARM_ID
					+ "" + sheduledSms.getId());
			SheduledAlaramManger.cancelAlaram(context, alarmId);

		} catch (Exception e) {
			// TODO: handle exception
		}
		SheduledAlaramManger.setAlaram(context, day, month, year, hour, minute,
				sheduledSms.getId());
	}

}
