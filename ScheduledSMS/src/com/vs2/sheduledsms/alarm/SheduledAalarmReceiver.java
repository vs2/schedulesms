package com.vs2.sheduledsms.alarm;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import com.vs2.scheduledsms.MainActivity;
import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.fragments.ScheduleFragment;
import com.vs2.sheduledsms.fragments.ScheduleFragment.getSheduledSmsList;
import com.vs2.sheduledsms.interfaces.SendSmsListener;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.utilities.Logcat;

public class SheduledAalarmReceiver extends BroadcastReceiver {

	private String tag = "SheduledAalarmReceiver";
	// private int notificationId = 111123;
	@SuppressWarnings("unused")
	private Context mContext;

	static SendSmsListener onCallBack;
	
	public static void setListner(SendSmsListener callback){
		onCallBack = callback;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		this.mContext = context;
		
		int SmsId = 0;
		
		if(intent.hasExtra(Globals.KEY_SMS_ID)){
			SmsId = intent.getIntExtra(Globals.KEY_SMS_ID, 0);
		}
		
		Logcat.e("onReceive notification alarm", "received"+" SmsId: "+SmsId);
		try {
			Logcat.e(tag, "context: " + context.toString());
			DatabaseFunctions.openDB(context);
			SheduledSms sheduleSms = DatabaseFunctions.getSmsFromId(SmsId);
			Logcat.e(tag, "sheduleSms: " + sheduleSms.toString());
		//	onCallBack= ScheduleFragment.mContext;
		//	onCallBack.sendsms(sheduleSms);
			MainActivity.sendsms1(sheduleSms);
			//ScheduleFragment.send_sms(sheduleSms,m);
		} catch (Exception e) {
			// TODO: handle exception
			Logcat.e(tag, "onReceive Error : " + e.toString());
		}		
				
	//	sendSMS(sheduleSms, mContext);
		

	}
	

}
