package com.vs2.sheduledsms.alarm;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.utilities.Logcat;

public class SheduledAlaramManger {

	private static final String tag = "NotifyAlaramManger";
	public static final int MINUTE = 60000;

	public static final int ALARM_ID = 121212;

	public static void setAlaram(Context context, int day, int month, int year,
			int hours, int minutes,int smsId) {

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, 0);
		
		int alaramId = Integer.parseInt(""+ALARM_ID+""+smsId);
		
		Intent intent = new Intent(context, SheduledAalarmReceiver.class);
		intent.putExtra(Globals.KEY_SMS_ID, smsId);
		PendingIntent sender = PendingIntent.getBroadcast(context, alaramId,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);				
		am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
	}

	public static void updateAlaram(Context context, int requestCode,
			long triggerAt, String carId, String carName) {
		Logcat.e(tag, "updateAlarm requestCode : " + requestCode);
		Intent intent = new Intent(context, SheduledAalarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		am.setRepeating(AlarmManager.RTC_WAKEUP, triggerAt, MINUTE, sender);
		Logcat.e("alaram", "alaram updated");
	}

	public static void cancelAlaram(Context context, int requestCode) {
		Intent intent = new Intent(context, SheduledAalarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		am.cancel(sender);
		Logcat.e("alaram", "alaram canceled for request code : " + requestCode);
	}
}
