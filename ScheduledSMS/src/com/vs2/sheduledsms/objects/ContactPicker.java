package com.vs2.sheduledsms.objects;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.vs2.sheduledsms.utilities.Logcat;


public class ContactPicker {

	private String id;
	private String number;
	private String name;
	private boolean checked;

	public ContactPicker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContactPicker(String id, String number, String name, boolean checked) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.checked = checked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public static ArrayList<ContactPicker> getContacts(Context context) {
		ArrayList<ContactPicker> contactPickers = new ArrayList<ContactPicker>();
		Cursor c = null;
		try {

			Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
			
			String[] projection = new String[] { Phone.CONTACT_ID,
					Phone.NUMBER,
					Phone.DISPLAY_NAME };
			
			/*String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
					ContactsContract.CommonDataKinds.Phone.NUMBER,
					ContactsContract.Contacts.DISPLAY_NAME };*/
			
			ContactPicker contact;

			c = context.getContentResolver().query(uri, projection, null, null,
					null);
			while (c.moveToNext()) {
				contact = new ContactPicker(c.getString(0), c.getString(1),
						c.getString(2), false);
				contactPickers.add(contact);
			}
			
			Logcat.e("Contact Picker List Size",""+contactPickers.size());
			
			return contactPickers;

		} catch (Exception e) {
			// TODO: handle exception
			Logcat.e("Get Contacts Error",""+e.toString());
			return contactPickers;
		} finally {
			c.close();
		}
	}
}
