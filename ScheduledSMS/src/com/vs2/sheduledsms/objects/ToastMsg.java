package com.vs2.sheduledsms.objects;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/*
 * This a custom expansion of android's default 
 * Toast message which acts like Crouton
 *  
 */
public class ToastMsg{
	
	/*
	 * Duration for ToastMsg to show
	 */
	public static final int LENGTH_SHORT = 1000;
	public static final int LENGTH_LONG = 2000;
	
	
	/*
	 * Gravity for ToastMsg to display
	 * on the screen 
	 */
	public static final int GRAVITY_TOP = 1;
	public static final int GRAVITY_CENTER = 2;
	public static final int GRAVITY_BOTTOM = 3;
	

	//Show default normal message with duration
	public static void show(Context context,String text,int duration,int gravity){
		buildToast(context, text, duration,Color.parseColor("#0099CC"),gravity);
	}
	
	//Show default normal message
	public static void show(Context context,String text,int gravity){
		if(gravity == LENGTH_LONG || gravity == LENGTH_SHORT){
			buildToast(context, text, gravity,Color.parseColor("#0099CC"),gravity);
		}else{
			buildToast(context, text, Toast.LENGTH_SHORT,Color.parseColor("#0099CC"),gravity);
		}
		
	}
	
	//Show error message with duration
	public static void showError(Context context,String text,int duration,int gravity){
		buildToast(context, text, duration,Color.parseColor("#CC0000"),gravity);
	}
	
	//Show error message
	public static void showError(Context context,String text,int gravity){
		if(gravity == LENGTH_LONG || gravity == LENGTH_SHORT){
			buildToast(context, text, gravity,Color.parseColor("#CC0000"),gravity);
		}else{
			buildToast(context, text, Toast.LENGTH_SHORT,Color.parseColor("#CC0000"),gravity);
		}
	}
	
	//Show success message with duration
	public static void showSuccess(Context context,String text,int duration,int gravity){
		buildToast(context, text, duration,Color.parseColor("#669900"),gravity);
	}
	
	//Show success message 
	public static void showSuccess(Context context,String text,int gravity){
		if(gravity == LENGTH_LONG || gravity == LENGTH_SHORT){
			buildToast(context, text, gravity,Color.parseColor("#669900"),gravity);
		}else{
			buildToast(context, text, Toast.LENGTH_SHORT,Color.parseColor("#669900"),gravity);
		}
		
	}
	
	//Show custom view 
	public static void showCustom(Context context,View customView,int gravity){
		if(gravity == LENGTH_LONG || gravity == LENGTH_SHORT){
			buildCustomToast(context,gravity, customView,gravity);
		}else{
			buildCustomToast(context,Toast.LENGTH_SHORT, customView,gravity);
		}
		
	}
	
	//Show custom view with duration
	public static void showCustom(Context context,View customView,int duration,int gravity){
		buildCustomToast(context,duration, customView,0);
	}
	
	//Build ToastMsg for normal view
	private static void buildToast(Context context,String text,int duration,int color,int gravity){
		Toast toast = new Toast(context);
		toast.setView(normalView(context,text,color));
		switch (gravity) {
		case GRAVITY_TOP:
			toast.setGravity(Gravity.TOP|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		case GRAVITY_CENTER:
			toast.setGravity(Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		case GRAVITY_BOTTOM:
			toast.setGravity(Gravity.BOTTOM|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		default:
			toast.setGravity(Gravity.TOP|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		}
		toast.setMargin(0f,0f);
		toast.setDuration(duration);
		toast.show();
	}
	
	//Build ToastMsg for custom view
	private static void buildCustomToast(Context context,int duration,View view,int gravity){
		Toast toast = new Toast(context);
		toast.setView(view);
		switch (gravity) {
		case GRAVITY_TOP:
			toast.setGravity(Gravity.TOP|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		case GRAVITY_CENTER:
			toast.setGravity(Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		case GRAVITY_BOTTOM:
			toast.setGravity(Gravity.BOTTOM|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		default:
			toast.setGravity(Gravity.TOP|Gravity.LEFT|Gravity.FILL_HORIZONTAL, 0, 0);
			break;
		}
		toast.setMargin(0f,0f);
		toast.setDuration(duration);
		toast.show();
	}
	
	
	//Create normal view layout for toast
	private static View normalView(Context context,String text,int color){
		
		RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		
		
		RelativeLayout layout = new RelativeLayout(context);
		layout.setBackgroundColor(color);
		layout.setPadding(15, 13, 15, 13);
		layout.setGravity(Gravity.TOP);
		layout.setLayoutParams(params);
		
		
		TextView textMessge =  new TextView(context);
		textMessge.setText(text);
		textMessge.setTextColor(Color.WHITE);
		layout.addView(textMessge);
		
		return layout;
	}

}
