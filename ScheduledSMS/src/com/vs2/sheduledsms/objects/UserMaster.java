package com.vs2.sheduledsms.objects;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class UserMaster {

	int id;
	String firstName,lastName,email,password;
	
	public UserMaster(int id, String firstName, String lastName, String email,
			String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public UserMaster() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static boolean saveUserMaster(UserMaster usermaster,Context context){
		if(usermaster == null)return false;
		
		try {
			PreferenceUser.setIntPref(PreferenceUser.KEY_ID, context, usermaster.getId());
			PreferenceUser.setStringPref(PreferenceUser.KEY_FIRST_NAME, context, usermaster.getFirstName());
			PreferenceUser.setStringPref(PreferenceUser.KEY_LAST_NAME, context, usermaster.getLastName());
			PreferenceUser.setStringPref(PreferenceUser.KEY_EMAIL, context, usermaster.getEmail());
			PreferenceUser.setStringPref(PreferenceUser.KEY_PASSWORD, context, usermaster.getPassword());
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Save user", "Error : " + e.toString());
		}
		return true;
	}
	
	public static UserMaster getUserMaster(JSONObject json){
		try {
			
			/**
			 * Using users object Changes the users of this User
			 */
			UserMaster userMaster = new UserMaster();
			if(json.has("Id")){
				userMaster.setId(json.getInt("Id"));
			}
			if(json.has("FirstName")){
				userMaster.setFirstName(json.getString("FirstName"));
			}
			if(json.has("LastName")){
				userMaster.setLastName(json.getString("LastName"));
			}
			if(json.has("Email")){
				userMaster.setEmail(json.getString("Email"));
			}
			
			if(json.has("Password")){
				userMaster.setPassword(json.getString("Password"));
			}
			return userMaster;

		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
public static boolean exists(Context context) {
		
		if(PreferenceUser.getStringPref(PreferenceUser.KEY_EMAIL, context) != null 
				&& PreferenceUser.getStringPref(PreferenceUser.KEY_EMAIL, context).length() > 0){
			return true;
		}else{
			return false;
		}
	}
public static UserMaster getSavedUser(Context context) {
	try {
		
		UserMaster userMaster = new UserMaster();
		userMaster.setId(PreferenceUser.getIntPref(PreferenceUser.KEY_ID, context));
		userMaster.setFirstName(PreferenceUser.getStringPref(PreferenceUser.KEY_FIRST_NAME, context));
		userMaster.setLastName(PreferenceUser.getStringPref(PreferenceUser.KEY_LAST_NAME, context));
		userMaster.setEmail(PreferenceUser.getStringPref(PreferenceUser.KEY_EMAIL, context));
		userMaster.setPassword(PreferenceUser.getStringPref(PreferenceUser.KEY_PASSWORD, context));
						
		return userMaster;
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("Get saved user", "Error : " + e.toString());
		return null;
	}
	
}

}
