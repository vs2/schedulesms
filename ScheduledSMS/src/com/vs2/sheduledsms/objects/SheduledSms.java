package com.vs2.sheduledsms.objects;

public class SheduledSms {

	private int Id;
	private String ContactId;
	private String smsTo;
	private String Date;
	private String Time;
	private String message;
	private int status;
	private boolean isSelect;
	
	public static String _Id = "Id";
	public static String _smsTo = "smsTo";
	public static String _Date = "Date";
	public static String _Time = "Time";
	public static String _message = "message";
	public static String _status = "status";
	public static String _contactId = "contactId";

	public SheduledSms(int id, String smsTo, String date, String time,
			String message, int status, boolean isSelect,String contactId) {
		super();
		Id = id;
		this.smsTo = smsTo;
		Date = date;
		Time = time;
		this.message = message;
		this.status = status;
		this.isSelect = isSelect;
		this.ContactId = contactId;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getSmsTo() {
		return smsTo;
	}

	public void setSmsTo(String smsTo) {
		this.smsTo = smsTo;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getContactId() {
		return ContactId;
	}

	public void setContactId(String contactId) {
		ContactId = contactId;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}

}
