package com.vs2.sheduledsms.objects;

public class Globals {

	public static final int TRUE = 1;
	public static final int FALSE = 0;

	public static final int IN_QUEUE = 1;
	public static final int SUCCESS = 2;
	public static final int FAILED = 3;
	public static final int ALL_RECORDS = 4;
	
	public static final String TEXT_INQUEUE = "In Queue";
	public static final String TEXT_SUCCESS = "Success";
	public static final String TEXT_FAILED = "Failed";
	
	
	public static final int SAVE = 1;
	public static final int UPDATE = 2;
	public static final int ERROR = 3;
	
	public static final String KEY_SMS_ID="smsId";
	
	public static final int SEND_SMS=4;
	
	//public static final String SERVER_PATH = "http://bb.vs2.in/schedulesms/";
	//public static final String SERVER_PATH = "http://textnice.com/";
	
	public static final String SERVER_PATH = "http://knouts.com/ScheduleSMS/";
	public static UserMaster sUserMaster;


}
