package com.vs2.sheduledsms.objects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Represents JSONParser
 */
public class JSONParser {
	
	  static InputStream is = null;
	    static JSONObject jObj = null;
	    static JSONArray jsonArray = null;
	    static String json = "";
	    
		static JSONObject jobject=null;
		
	    public JSONParser() {
	 
	    }
	    /**
	     * Getting JSONObject from Server
	     * @param s
	     * @return JSONObject
	     */
		public JSONObject makeHttpRequest(String s) {
			try {
				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(s);

				HttpResponse httpresponse = httpclient.execute(httppost);
				HttpEntity httpentry = httpresponse.getEntity();
				is = httpentry.getContent();

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(is==null)
				{
					Log.e("Class", "InputStream null");
				}else
				{
					Log.e("Class", "InputStream not null");
				}
				BufferedReader bfrreader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder strbldr = new StringBuilder();
				String str = null;
				
				try {
					while ((str = bfrreader.readLine()) != null) {
						strbldr.append(str + "\n");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				strbldr.append(str + "\n");
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				json = strbldr.toString();
				
				try {
					jobject = new JSONObject(json);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return jobject;

		}
	 
		 /**
	     * Getting JSONObject from Server
	     * @param s
	     * @return JSONObject
	     */
	    public JSONObject getJSONFromUrl(String url,List<NameValuePair> params) {

	        // Making HTTP request
	        try {
	            // defaultHttpClient
	        	Log.e("Api JSOn", params.toString());
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            HttpPost httpPost = new HttpPost(url);
	            
	            // Request parameters and other properties.
	            if(params != null && params.size() > 0){
	            	httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
	            }
	 
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            is = httpEntity.getContent();           
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "iso-8859-1"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	            }
	            is.close();
	            json = sb.toString();
	            Log.e("JsonString", "Json " + json);
	        } catch (Exception e) {
	            Log.e("Buffer Error", "Error converting result " + e.toString());
	        }
	 
	        // try parse the string to a JSON object
	        try {
	            jObj = new JSONObject(json);
	        } catch (JSONException e) {
	            //Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }
	 
	        // return JSON String
	       // Log.e("JSON OBJECT",jObj.toString());
	        return jObj;
	        
	 
	    }
	    /**
	     * Getting JSONArray from Server
	     * @param s
	     * @return JSONArray
	     */
	    public JSONArray getJSONArrayFromUrl(String url) {

	        // Making HTTP request
	        try {
	            // defaultHttpClient
				
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            HttpPost httpPost = new HttpPost(url);
	                  
	 
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            is = httpEntity.getContent();           
	 
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        try {
	            //BufferedReader reader = new BufferedReader(new InputStreamReader(
	              //      is, "iso-8859-1"), 8);
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "UTF-8"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                //sb.append(line + "\n");
	            	sb.append(line);
	            }
	            is.close();
	            json = sb.toString();
	        } catch (Exception e) {
	            //Log.e("Buffer Error", "Error converting result " + e.toString());
	        }
	 
	        // try parse the string to a JSON object
	        try {
	            jsonArray = new JSONArray(json);
	        } catch (JSONException e) {
	            //Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }
	 
	        // return JSON String
	        return jsonArray;
	 
	    }


}
