package com.vs2.sheduledsms.objects;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUser {
	

	public static final String PREFS_SETTINGS = "UserMaster_ScheduleSMS";
	
	public static final String KEY_ID = "Id";
	public static final String KEY_FIRST_NAME = "FirstName";
	public static final String KEY_LAST_NAME = "LastName";
	public static final String KEY_EMAIL = "Email";
	public static final String KEY_PASSWORD = "Password";
	public static void clearPreference(Context context){
		SharedPreferences sharedPrefs = context.getSharedPreferences(PREFS_SETTINGS, 0);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.commit();
	}
	
	public static void setBooleanPref(String prefKey,Context context,boolean status){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putBoolean(prefKey, status);
	      editor.commit();
	}
	
	public static boolean getBooleanPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	       return settings.getBoolean(prefKey, false);
	}
	
	public static void setIntPref(String prefKey,Context context,int value){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putInt(prefKey, value);
	      editor.commit();
	}
	
	public static int getIntPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	    return settings.getInt(prefKey, 40);
	}
	
	public static void setStringPref(String prefKey,Context context,String value){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString(prefKey, value);
	      editor.commit();
	}
	
	public static String getStringPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_SETTINGS, 0);
	    return settings.getString(prefKey,null);
	}
	
	
	
}
