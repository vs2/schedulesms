package com.vs2.sheduledsms.utilities;

import java.io.ByteArrayInputStream;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.widget.EditText;
import android.widget.Toast;

public class Utility {

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm == null || cm.getActiveNetworkInfo() == null) ? false : cm
				.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	/*
	 * public static void loadAds(boolean showAds, AdView adView) { // Look up
	 * the AdView as a resource and load a request. // AdView adView =
	 * (AdView)this.findViewById(R.id.adView); adView.setVisibility(View.GONE);
	 * if (showAds) { AdRequest adRequest = new AdRequest.Builder().build();
	 * adView.loadAd(adRequest); adView.setVisibility(View.VISIBLE); } else {
	 * adView.setVisibility(View.GONE); } }
	 */
	public static boolean isPasswordConfirm(Context context,
			EditText edittextPassword, EditText edittextConfirmPassword) {
		try {

			String password, confirmPassword;

			password = edittextPassword.getText().toString().trim();
			confirmPassword = edittextConfirmPassword.getText().toString()
					.trim();

			if (password.equals(confirmPassword)) {
				return true;
			} else {
				Toast.makeText(context,
						"Password not match please reenter password",
						Toast.LENGTH_LONG).show();
				return false;
			}

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public static Bitmap getByteContactPhoto(String contactId, Context context) {
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI,
				Long.parseLong(contactId));
		Uri photoUri = Uri.withAppendedPath(contactUri,
				Contacts.Photo.CONTENT_DIRECTORY);
		Cursor cursor = context.getContentResolver().query(photoUri,
				new String[] { Contacts.Photo.DATA15 }, null, null, null);
		if (cursor == null) {
			return null;
		}
		try {
			if (cursor.moveToFirst()) {
				byte[] data = cursor.getBlob(0);
				if (data != null) {
					Bitmap bitmap = BitmapFactory
							.decodeStream(new ByteArrayInputStream(data));

					// bitmap = Bitmap.createScaledBitmap(bitmap, 80, 80,
					// false);
					return bitmap;
				}
			}
		} finally {
			cursor.close();
		}

		return null;
	}

}
