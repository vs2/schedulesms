package com.vs2.sheduledsms.dialogs;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.adpaters.ContactListAdapter;
import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.interfaces.onSelectContact;
import com.vs2.sheduledsms.objects.ContactPicker;

public class ContactListDialog extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dailog;
	public ListView listContact;
	public ImageButton imageButtonExit;
	public EditText mEditTextSearch;
	public static onSelectContact contactListener;

	public static ArrayList<ContactPicker> mContactPickerList;

	public FragmentManager mFragmentManager;

	public ContactListAdapter adapter;

	public ContactListDialog(Activity activity) {
		super(activity);
		// TODO Auto-generated constructor stub
		this.activity = activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.contact_list_dialog);
		try {
			DatabaseFunctions.openDB(activity);
		} catch (Exception e) {
			// TODO: handle exception
		}

		new getContactList().execute();

		listContact = (ListView) findViewById(R.id.listViewContact);
		imageButtonExit = (ImageButton) findViewById(R.id.imageButtonClose);
		mEditTextSearch = (EditText) findViewById(R.id.edittext_search);
		imageButtonExit.setOnClickListener(this);

		listContact.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				contactListener.getSelectedContact(mContactPickerList
						.get(position));
				dismiss();
			}
		});

		mEditTextSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		contactListener = (onSelectContact) activity;
		super.onAttachedToWindow();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.imageButtonClose) {
			dismiss();
		}
	}

	public void loadContactList() {
		adapter = new ContactListAdapter(activity, mContactPickerList);
		listContact.setAdapter(adapter);
	}

	public class getContactList extends
			AsyncTask<Void, Void, ArrayList<ContactPicker>> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(activity);
			pd.setMessage("Please wait, getting data");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected ArrayList<ContactPicker> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				return ContactPicker.getContacts(activity);
			} catch (Exception e) {
				// TODO: handle exception
				return null;
			}

		}

		@Override
		protected void onPostExecute(ArrayList<ContactPicker> result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				if (result != null && result.size() > 0) {
					mContactPickerList = result;
					loadContactList();
				} else {
					/*Log.e("","length:"+mContactPickerList.size());
					mEditTextSearch.setVisibility(View.GONE);
					ToastMsg.showError(getOwnerActivity(), "There are no contects in phone", ToastMsg.GRAVITY_CENTER);*/
					contactListener.getSelectedContact(null);
				}
				
				//Log.e("","length:"+mContactPickerList.size());
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

}
