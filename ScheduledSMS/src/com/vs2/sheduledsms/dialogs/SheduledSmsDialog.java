package com.vs2.sheduledsms.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vs2.scheduledsms.R;
import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.fragments.DatePickerFragment;
import com.vs2.sheduledsms.fragments.TimePickerFragment;
import com.vs2.sheduledsms.interfaces.OnSmsDialogClick;
import com.vs2.sheduledsms.objects.ContactPicker;
import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.SheduledSms;

public class SheduledSmsDialog extends Dialog implements
		android.view.View.OnClickListener, android.view.View.OnKeyListener {

	public Activity activity;
	public Dialog dailog;
	public Button buttonSave, buttonCancel, buttonContact;
	public static EditText edittextSmsTo,
			edittextMessage;
	public static TextView textViewTitle,textViewDate,textViewTime;
	public static OnSmsDialogClick smsDialogClick;
	public static SheduledSms sms;
	public int actionType = 0;
	public int smsId;

	public static int day = 0;
	public static int month = 0;
	public static int year = 0;

	public static int hour = 0;
	public static int minute = 0;

	public FragmentManager mFragmentManager;
	
	public String contactId;

	public SheduledSmsDialog(Activity activity) {
		super(activity);
		// TODO Auto-generated constructor stub
		this.activity = activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sheduledsms_dialog);
		
		try {
			DatabaseFunctions.openDB(activity);
		} catch (Exception e) {
			// TODO: handle exception
		}
		buttonSave = (Button) findViewById(R.id.buttonSave);
		buttonCancel = (Button) findViewById(R.id.buttonCancel);
		buttonContact = (Button) findViewById(R.id.buttonContact);
		edittextSmsTo = (EditText) findViewById(R.id.edittext_smsto);
		
		textViewDate = (TextView) findViewById(R.id.textview_date);
		
		textViewTime = (TextView) findViewById(R.id.textview_time);
		edittextMessage = (EditText) findViewById(R.id.edittext_message);
		textViewTitle = (TextView) findViewById(R.id.textViewTitle);
		buttonSave.setOnClickListener(this);
		buttonCancel.setOnClickListener(this);
		buttonContact.setOnClickListener(this);
		textViewDate.setOnClickListener(this);
		textViewTime.setOnClickListener(this);
		edittextSmsTo.setOnClickListener(this);
		edittextSmsTo.setOnKeyListener(this);
		edittextMessage.setOnKeyListener(this);
	}

	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		smsDialogClick = (OnSmsDialogClick) activity;
		super.onAttachedToWindow();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonSave:
			try {
				if (checkValidation()) {
					String phoneNoString = edittextSmsTo.getText().toString()
							.trim();
					String date = textViewDate.getText().toString().trim();
					String time = textViewTime.getText().toString().trim();
					String message = edittextMessage.getText().toString()
							.trim();

					SheduledSms sms = new SheduledSms(0, phoneNoString, date,
							time, message, Globals.IN_QUEUE, false,contactId);

					if (actionType == Globals.SAVE) {
						if (DatabaseFunctions.saveSheduledSms(phoneNoString,
								date, time, message, Globals.IN_QUEUE,contactId)) {
							sms = DatabaseFunctions.getSmsMaxRecord();
							smsDialogClick.onDialogClick(Globals.SAVE, sms);
						} else {
							smsDialogClick.onDialogClick(Globals.ERROR, sms);
						}
					} else {
						sms = new SheduledSms(smsId, phoneNoString, date, time,
								message, Globals.IN_QUEUE, false,contactId);
						if (DatabaseFunctions.updateSheduleSms(smsId,phoneNoString, date,
								time, message)) {
							sms = DatabaseFunctions.getSmsMaxRecord();
							smsDialogClick.onDialogClick(Globals.UPDATE, sms);
						} else {
							smsDialogClick.onDialogClick(Globals.ERROR, sms);
						}
					}
					dismiss();
				} 
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case R.id.buttonCancel:
			// dialogDestroy.OnContactDialogOptionSelect(Global.CANCEL);
			dismiss();
			break;
		case R.id.textview_date:
			
			showDatePickerDialog();
			break;
		case R.id.textview_time:
			
			showTimePickerDialog();
			break;
		case R.id.edittext_smsto:
			
			break;
		case R.id.buttonContact:
			ContactListDialog contactDialog = new ContactListDialog(activity);
			contactDialog.show();
			break;
		default:
			break;
		}
	}

	public void setType(int actiontype, SheduledSms sms,
			FragmentManager fragmentManager) {

		mFragmentManager = fragmentManager;

		actionType = actiontype;
		if (actionType == Globals.SAVE) {
			buttonSave.setText("SAVE");
			textViewTitle.setText(activity.getResources().getString(
					R.string.add_sheduled_sms));
		} else {
			buttonSave.setText("UPDATE");
			textViewTitle.setText(activity.getResources().getString(
					R.string.edit_sheduled_sms));
			edittextSmsTo.setText("" + sms.getSmsTo());
			textViewDate.setText("" + sms.getDate());
			textViewTime.setText("" + sms.getTime());
			edittextMessage.setText("" + sms.getMessage());
			smsId = sms.getId();
		}
	}

	private boolean checkValidation() {
		if (edittextSmsTo.getText().toString().trim().length() == 0) {
			Toast.makeText(activity, "Please enter Number", Toast.LENGTH_LONG)
					.show();
			return false;
		} else if (textViewDate.getText().toString().trim().length() == 0 || textViewDate.getText().toString().trim().toLowerCase().contains("set")) {
			Toast.makeText(activity, "Please select scheduled date",
					Toast.LENGTH_LONG).show();
			return false;
		} else if (textViewTime.getText().toString().trim().length() == 0 || textViewTime.getText().toString().trim().toLowerCase().contains("set")	) {
			Toast.makeText(activity, "Please select scheduled time",
					Toast.LENGTH_LONG).show();
			return false;
		} else if (edittextMessage.getText().toString().trim().length() == 0) {
			Toast.makeText(activity, "Please enter your message",
					Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	protected void showTimePickerDialog() {
		// TODO Auto-generated method stub
		DialogFragment timePickerFragment = new TimePickerFragment();
		timePickerFragment.show(mFragmentManager, "Choose Scheduled time");
	}

	protected void showDatePickerDialog() {
		DialogFragment datePickerFragment = new DatePickerFragment();
		
		datePickerFragment.show(mFragmentManager, "Choose Scheduled Date");
	}

	public static void setDOB() {
		textViewDate.setText(day + "-" + month + "-" + year);
		
	}

	public static void setTime() {
		String min;
		if(minute<10){
			min="0"+minute;
		}else{
			min=""+minute;
		}
		textViewTime.setText(hour + ":" + min);
	}

	public void setContactNumber(ContactPicker contactPicker) {
		contactId = contactPicker.getId();
		edittextSmsTo.setText("" + contactPicker.getNumber());
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.edittext_smsto:
			if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		               (keyCode == KeyEvent.KEYCODE_ENTER))
		          {
		                // Perform action on Enter key press
		                edittextSmsTo.clearFocus();
		                edittextMessage.requestFocus();
		                
		                return true;
		          }
		          return false;
		
			
		case R.id.edittext_message:
			if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER))
          {
				showDatePickerDialog();
                return true;
          }
          return false;
		}
		return false;
	}
}
