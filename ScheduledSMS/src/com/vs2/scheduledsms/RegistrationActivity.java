package com.vs2.scheduledsms;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.JSONParser;
import com.vs2.sheduledsms.objects.PreferenceUser;
import com.vs2.sheduledsms.objects.ToastMsg;
import com.vs2.sheduledsms.objects.UserMaster;
import com.vs2.sheduledsms.utilities.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class RegistrationActivity extends Activity {

	private EditText mEdittextFirstName,mEdittextLastName,mEdittextPassword,mEdittextRePasswrod,mEdittextEmail;
	private Button mButtonSubmit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_registration);
		initGloble();
		
	}
	
	public void initGloble(){
		
		mEdittextFirstName=(EditText)findViewById(R.id.edittext_first_name);
		mEdittextLastName=(EditText)findViewById(R.id.edittext_last_name);
		mEdittextPassword=(EditText)findViewById(R.id.edittext_password);
		mEdittextRePasswrod=(EditText)findViewById(R.id.edittext_repassword);
		mEdittextEmail=(EditText)findViewById(R.id.edittext_emailid);
		mButtonSubmit=(Button)findViewById(R.id.button_submit);
		
		mButtonSubmit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(notBlank(mEdittextFirstName) && notBlank(mEdittextLastName)
						&& notBlank(mEdittextEmail) && notBlank(mEdittextPassword) && notBlank(mEdittextRePasswrod)){
					
					if (Utility.isOnline(RegistrationActivity.this)) {
						
						if(mEdittextPassword.getText().toString().equals(mEdittextRePasswrod.getText().toString())){
														
							new RegisterApi().execute(mEdittextFirstName.getText().toString(),
									mEdittextLastName.getText().toString(),
									mEdittextEmail.getText().toString(), mEdittextPassword.getText().toString());
						}else{
							ToastMsg.showError(RegistrationActivity.this,
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
					} else {
						Log.e( "No connection", "No internet connection found!");
						ToastMsg.showError(RegistrationActivity.this,"No internet connection found!", ToastMsg.GRAVITY_TOP);
					}
					
				}
				}
				
		});
		
	}
	public class RegisterApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(RegistrationActivity.this);
			pd.setMessage("Registering please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("FirstName", params[0]));
			postParams.add(new BasicNameValuePair("LastName", params[1]));
			postParams.add(new BasicNameValuePair("Email", params[2]));
			postParams.add(new BasicNameValuePair("Password", params[3]));
		
			try {
				
				return parser.getJSONFromUrl(Globals.SERVER_PATH
						+ "user_register.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("RegistrationApi", "Error : " + e.toString());
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON",result+"");
				if (result != null) {
					processRegistration(result);
				} else {
					Log.e("Registration", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	
	private void processRegistration(JSONObject result){
		
		if(result.has("Status") && result.has("Message")){
			try {
				String message = result.getString("Message");
				if(result.getInt("Status") == 1){
					PreferenceUser.clearPreference(RegistrationActivity.this);	
					Globals.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));										
					UserMaster.saveUserMaster(Globals.sUserMaster,RegistrationActivity.this);
					Intent intent = new Intent(RegistrationActivity.this,MainActivity.class);
					startActivity(intent);
					finish();
				}else{
					ToastMsg.showError(RegistrationActivity.this,
							message, ToastMsg.GRAVITY_TOP);
					
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Registration", "Registration error");
			}
		}else{
			ToastMsg.showError(RegistrationActivity.this,
					"Unable to Register", ToastMsg.GRAVITY_TOP);
		}
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
}
