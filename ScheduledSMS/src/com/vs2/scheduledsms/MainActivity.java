package com.vs2.scheduledsms;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.vs2.sheduledsms.adpaters.TabPagerAdapter;
import com.vs2.sheduledsms.database.DatabaseFunctions;
import com.vs2.sheduledsms.dialogs.SheduledSmsDialog;
import com.vs2.sheduledsms.fragments.ScheduleFragment;
import com.vs2.sheduledsms.fragments.SentFragment;
import com.vs2.sheduledsms.interfaces.OnSmsDialogClick;
import com.vs2.sheduledsms.interfaces.SendSmsListener;
import com.vs2.sheduledsms.interfaces.onSelectContact;
import com.vs2.sheduledsms.objects.ContactPicker;
import com.vs2.sheduledsms.objects.SheduledSms;
import com.vs2.sheduledsms.utilities.Logcat;

public class MainActivity extends SherlockFragmentActivity implements
		OnSmsDialogClick, SendSmsListener, onSelectContact {

	public static SheduledSmsDialog smsDialog;
	ViewPager Tab;
	com.vs2.sheduledsms.adpaters.TabPagerAdapter TabAdapter;
	ActionBar actionbar;
	
	public static Context context ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TabAdapter = new TabPagerAdapter(getSupportFragmentManager());
		Tab = (ViewPager) findViewById(R.id.pager);
		context = MainActivity.this;

		try {
			DatabaseFunctions.openDB(MainActivity.this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Tab.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

				actionbar = getSherlock().getActionBar();
				actionbar.setSelectedNavigationItem(position);
				if(position == 0){
					actionbar.setTitle("Schedule SMS");
				}else{
					actionbar.setTitle("Sent SMS");
					new SentFragment.getSheduledSmsList().execute();
				}
				
			}
		});
		Tab.setAdapter(TabAdapter);
		actionbar = getSherlock().getActionBar();
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabUnselected(Tab tab,
					android.support.v4.app.FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTabSelected(Tab tab,
					android.support.v4.app.FragmentTransaction ft) {
				// TODO Auto-generated method stub
				Log.e("tab.getPosition()",""+tab.getPosition());
				Tab.setCurrentItem(tab.getPosition());

			}

			@Override
			public void onTabReselected(Tab tab,
					android.support.v4.app.FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}
		};

		actionbar.setStackedBackgroundDrawable(new ColorDrawable(getResources()
				.getColor(R.color.tab_background_color)));

		actionbar.addTab(actionbar.newTab().setText("Scheduled")
				.setTabListener(tabListener));
		actionbar.addTab(actionbar.newTab().setText("Sent")
				.setTabListener(tabListener));
		setActionBar();
		

	}
	

	private void setActionBar() {
		
		actionbar.setBackgroundDrawable(MainActivity.this.getResources()
				.getDrawable(R.drawable.actionbar_bg));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		
		  MenuInflater inflater = this.getSupportMenuInflater();
		  inflater.inflate(R.menu.addnew_action_menu, menu);
		 
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*if (item.getItemId() == R.id.action_add) {
			smsDialog = new SheduledSmsDialog(MainActivity.this);
			smsDialog.show();
			smsDialog.setType(Globals.SAVE, null, getSupportFragmentManager());
		}*/
		if (item.getItemId() == R.id.action_edit) {

		}
		if (item.getItemId() == R.id.action_delete) {

		}
		if (item.getItemId() == R.id.action_logout) {
			com.vs2.sheduledsms.objects.PreferenceUser.clearPreference(MainActivity.this);
			startActivity(new Intent(MainActivity.this,
					LoginActivity.class));
			finish();
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDialogClick(int type, SheduledSms sheduledSms) {
		// TODO Auto-generated method stub
		ScheduleFragment.onDialog_Click(type, sheduledSms);
	}

	@Override
	public void sendsms(SheduledSms sheduledSms) {
		// TODO Auto-generated method stub
		Logcat.e("Main sheduledSms", "main sheduleSms: " + sheduledSms.toString());
		ScheduleFragment.send_sms(sheduledSms, MainActivity.this);
	}
	
	public static void sendsms1(SheduledSms sheduledSms) {
		// TODO Auto-generated method stub
		Logcat.e("Main sheduledSms", "main sheduleSms: " + sheduledSms.toString());
		ScheduleFragment.send_sms(sheduledSms, context);
	}

	@Override
	public void getSelectedContact(ContactPicker contact) {
		// TODO Auto-generated method stub
		ScheduleFragment.getSelected_Contact(contact);
	}

}
