package com.vs2.scheduledsms;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.vs2.sheduledsms.fragments.DatePickerFragment;
import com.vs2.sheduledsms.fragments.TimePickerFragment;

public class TimeActivity extends SherlockFragmentActivity {

	public static int day = 0;
	public static int month = 0;
	public static int year = 0;

	public static int hour = 0;
	public static int minute = 0;

	private static TextView mTextViewDate;
	private static TextView mTextViewTime;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_timer);
		inilGlobal();
	}

	private void inilGlobal(){
		mTextViewDate = (EditText) findViewById(R.id.textview_date);
		mTextViewTime = (EditText) findViewById(R.id.textview_time);
		
		mTextViewDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDatePickerDialog();
			}
		});
		
		mTextViewTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTimePickerDialog();
			}
		});
		
	}
	
	protected void showTimePickerDialog() {
		// TODO Auto-generated method stub
		DialogFragment timePickerFragment = new TimePickerFragment();
		timePickerFragment.show(getSupportFragmentManager(),
				"Choose Sheduled time");
	}
	
	protected void showDatePickerDialog()
	{
		DialogFragment datePickerFragment = new DatePickerFragment();
		datePickerFragment.show(getSupportFragmentManager(), "Choose Sheduled Date");
	}
	public static void setDOB() {
		mTextViewDate.setText(day + "-" + month + "-" + year);
	}

	public static void setTime() {
		mTextViewTime.setText(hour + ":" + minute);
	}
}
