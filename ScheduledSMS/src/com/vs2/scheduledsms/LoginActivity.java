package com.vs2.scheduledsms;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.vs2.sheduledsms.objects.Globals;
import com.vs2.sheduledsms.objects.JSONParser;
import com.vs2.sheduledsms.objects.ToastMsg;
import com.vs2.sheduledsms.objects.UserMaster;
import com.vs2.sheduledsms.utilities.Utility;

public class LoginActivity extends Activity {

	private Button mButtonLogin,mButtonRegistration,mButtonForgetPassword;
	private EditText mEdittextEmail,mEdittextPass;
	ArrayList<UserMaster> arry_usermaster;
	int MYID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		
		mButtonLogin=(Button)findViewById(R.id.button_login);
		mButtonRegistration=(Button)findViewById(R.id.button_registration);
		mButtonForgetPassword=(Button)findViewById(R.id.button_forgetpass);
		mEdittextEmail=(EditText)findViewById(R.id.edittext_email);
		mEdittextPass=(EditText)findViewById(R.id.edittext_password);
		
		if(UserMaster.exists(LoginActivity.this)){
			
			Globals.sUserMaster = UserMaster.getSavedUser(LoginActivity.this);
			startActivity(new Intent(LoginActivity.this,MainActivity.class));
			finish();
		}
		
		mButtonLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				if(notBlank(mEdittextEmail) && notBlank(mEdittextPass)){

					if (Utility.isOnline(LoginActivity.this)) {
						
						new LoginApi().execute(mEdittextEmail.getText().toString().trim(),
								mEdittextPass.getText().toString().trim());
					} else {
						Log.e( "No connection", "No internet connection found!");
						ToastMsg.showError(LoginActivity.this,"No internet connection found!", ToastMsg.GRAVITY_TOP);
					}

				} 				
			}
				
			});
		mButtonRegistration.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
                        RegistrationActivity.class);
                startActivity(i);
			}
		});
		mButtonForgetPassword.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog();
			}
		});
	}
	@SuppressLint("NewApi")
	public void showDialog() {
		LayoutInflater adbInflater = LayoutInflater.from(LoginActivity.this);
		View agreeView = adbInflater.inflate(R.layout.forget_password, null);
		final EditText edittextClassName = (EditText) agreeView
				.findViewById(R.id.edittext_classname);
		
		if(mEdittextEmail.getText().toString().equals("")){
			edittextClassName.setHint("Your Email Address");
		}else{
			edittextClassName.setText(mEdittextEmail.getText().toString());
		}
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.CustomAlertDialog);
		builder.setTitle("Forget Password");
		builder.setInverseBackgroundForced(true);
		builder.setView(agreeView);
		builder.setPositiveButton("Send me Password", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (edittextClassName.getText().toString().equals("")) {
					ToastMsg.showError(LoginActivity.this,
							"You Must Enter Email Address",
							ToastMsg.GRAVITY_CENTER);
				} else {

					if (Utility.isOnline(LoginActivity.this)) {

						new GetingPasswordApi().execute(edittextClassName.getText().toString());
					} else {
						Log.e("No connection", "No internet connection found!");
						ToastMsg.showError(LoginActivity.this,
								"No internet connection found!",
								ToastMsg.GRAVITY_CENTER);
					}

				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.show();
	}
	
	public class LoginApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			pd = new ProgressDialog(LoginActivity.this);
			pd.setMessage("Logging please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(true);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("Email", params[0]));
			postParams.add(new BasicNameValuePair("Password", params[1]));
			try {
				
				return parser.getJSONFromUrl(Globals.SERVER_PATH
						+ "user_login.php",postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("LoginApi", "Error : " + e.toString());
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("",result+"");
				if (result != null) {
					processLogin(result);
				} else {
					Log.e("Login", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	
	private void processLogin(JSONObject result){
		if(result.has("Status") && result.has("Message")){
			try {
				if(result.getInt("Status") == 1){
					Globals.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));					
					UserMaster.saveUserMaster(Globals.sUserMaster,LoginActivity.this);
					Intent intent = new Intent(LoginActivity.this,MainActivity.class);
					startActivity(intent);
					finish();
				}else{
					ToastMsg.showError(LoginActivity.this,"Wrong password or username", ToastMsg.GRAVITY_TOP);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Login","Login error");
			}
		}else{
			//Toast.makeText(MainActivity.this, "Unable to Login", Toast.LENGTH_LONG).show();
			ToastMsg.showError(LoginActivity.this,"Unable to Login", ToastMsg.GRAVITY_TOP);
		}
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
	
	public class GetingPasswordApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			pd = new ProgressDialog(LoginActivity.this);
			pd.setMessage("Please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(true);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("Email", params[0]));
			try {
				
				return parser.getJSONFromUrl(Globals.SERVER_PATH
						+ "forget_password.php",postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("GetingPasswordApi", "Error : " + e.toString());
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("",result+"");
				if (result != null) {
					processGeting(result);
				} else {
					Log.e("GetingPasswordApi", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processGeting(JSONObject result){
		if(result.has("Status") && result.has("Message")){
			try {
				String Message=result.getString("Message");
				if(result.getInt("Status") == 1){
					ToastMsg.showSuccess(LoginActivity.this,
							Message, ToastMsg.GRAVITY_TOP);
				}else{
					ToastMsg.showError(LoginActivity.this,Message, ToastMsg.GRAVITY_TOP);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("GetingPasswordApi","GetingPasswordApi error");
			}
		}else{
			ToastMsg.showError(LoginActivity.this, "Unable to Geting Password", ToastMsg.GRAVITY_TOP);
		}
	}
	
}
